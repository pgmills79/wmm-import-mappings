--DECLARE @trans_audit_x29 VARCHAR(40) = 'wmm_audit_x29_insert'
DECLARE @sessionID BIGINT = 0
DECLARE @status INT = 0
DECLARE @errormesssage VARCHAR(MAX)
-- DECLARE @trans_wmm_vintage_record_insert VARCHAR(40) = 'wmm_vintage_record_insert'
-- DECLARE @trans_vintage_spfu_insert VARCHAR(40) = 'vintage_spfu_insert'
-- DECLARE @trans_vintage_spfv_insert VARCHAR(40) = 'vintage_spfv_insert'
-- DECLARE @trans_vintage_spfw_insert VARCHAR(40) = 'vintage_spfw_insert'
-- DECLARE @trans_vintage_sphs_insert VARCHAR(40) = 'vintage_sphs_insert'



/*
	****************************************************
		 Insert a x29 Audit Table record.....
		 We want to keep track of the records we have inserted
   ****************************************************
*/
BEGIN TRY
   
           -- BEGIN TRANSACTION @trans_audit_x29
			
			INSERT INTO spdsdev.dbo.X29_AccessorSessions
					( AccessorSource ,
					  SourceId ,
					  Active ,
					  SessionKey ,
					  DatabaseId ,
					  StartDateTime ,
					  EndDateTime
					)
			VALUES  ( 'I' ,				-- AccessorSource - char(1)
					  'vintage_service_trip_insert' ,-- SourceId - varchar(200)
					  1,				-- Active - bit
					  NEWID(),			-- SessionKey - uniqueidentifier
					  'SPDSDEV' ,		-- DatabaseId - varchar(10)
					  GETDATE() ,		-- StartDateTime - datetime
					  GETDATE()			-- EndDateTime - datetime
					);

			--GET THE SESSION IDS FOR LATER AUDIT INSERTS
			SET @sessionID = SCOPE_IDENTITY();
			
			-- IF @@TRANCOUNT > 0
				-- COMMIT TRANSACTION @trans_audit_x29			 
		
		
	END TRY

	BEGIN CATCH
		
		SET @status = 1
		SET @errormesssage = char(13) + @errormesssage + 'Error in  = ' + ERROR_PROCEDURE() + ', procedure line ' + 
									   RTRIM(CONVERT(CHAR(10), ERROR_LINE())) + ', ' +
									   ERROR_MESSAGE() --+ char(13) +
									   --'Transaction : ' + @trans_audit_x29
END CATCH

/*
   ****************************************************
	 END: Insert a x29 Audit Table record.....
   ****************************************************

*/



/*
	****************************************************
	   INSERT INTO SPFT TABLE.....
	   First, we need to insert into the primary Service Trip Table (SPFT)
	   We need to get the record id inserted for this to update the 
	   other child entitiy tables with the correct spft record id
   ****************************************************
*/

BEGIN TRY
	
	  --BEGIN TRANSACTION @trans_wmm_vintage_record_insert

			DECLARE @inserted_spft TABLE
			(
				recordid BIGINT
			);

			WITH import_into_spft (	SpecialtyCode, PlacementCoordinator, 
									LogisticsCoordinator, AccountNumber,
									TripStatus, TravelStatus,
									PrimaryDoctor, Country,
									PrimaryTripID, SPGVRecordId)
			AS
			(

			   SELECT v_spft.donor_SpecialityCode,
							v_spft.donor_PlacementCoordinator,
							v_spft.donor_LogisticsCoordinator, 
						v_spft.donor_AccountNumber,
						v_spft.donor_TripStatus, 
						v_spft.donor_TravelStatus,
						v_spft.donor_PrimaryDoctorID, 
						v_spft.donor_Country,
						v_spft.donor_PrimaryTripID, 
						v_spft.donor_SPGVRecordID
			   FROM dbo.wmm_vintage_to_spft v_spft 
			   /**We want to make sure the account number exists in the account master table */
			   WHERE EXISTS (	
								SELECT 1 
								FROM SPDSDEV.dbo.A01_AccountMaster am
								WHERE am.AccountNumber = v_spft.donor_AccountNumber
							) 
				AND v_spft.donor_AccountNumber IS NOT NULL 
				--we only want to insert records that dont have an SPFT record already (i.e, they have been inserted)
				AND v_spft.donor_SPFTrecordID IS NULL 
				AND NOT EXISTS  (SELECT 1
								 FROM dbo.wmm_service_trip_imports import
								 WHERE import.SPFT_RecordID = v_spft.donor_SPFTrecordID
								)
				GROUP BY	v_spft.donor_SpecialityCode,
							v_spft.donor_PlacementCoordinator,
							v_spft.donor_LogisticsCoordinator, 
							v_spft.donor_AccountNumber,
							v_spft.donor_TripStatus, 
							v_spft.donor_TravelStatus,
							v_spft.donor_PrimaryDoctorID, 
							v_spft.donor_Country,
							v_spft.donor_PrimaryTripID, 
							v_spft.donor_SPGVRecordID
			)
			INSERT INTO SPDSDEV.dbo.SPFT_WMMServiceTrips
			(
				SpecialtyCode,
				PlacementCoordinator,
				LogisticsCoordinator,
				AccountNumber,
				TripStatus,
				TravelStatus,
				PrimaryDoctor,
				Country,
				PrimaryTripId,
				SPGVRecordId
			)
			OUTPUT inserted.RecordId
			INTO @inserted_spft	
			SELECT i_spft.SpecialtyCode,
					i_spft.PlacementCoordinator,
					i_spft.LogisticsCoordinator,
					i_spft.AccountNumber,
					i_spft.TripStatus,
					i_spft.TravelStatus,
					i_spft.PrimaryDoctor,
					i_spft.Country,
					 i_spft.PrimaryTripID,
					i_spft.SPGVRecordId 
			FROM import_into_spft i_spft
			EXCEPT 
			SELECT	st.SpecialtyCode,
					st.PlacementCoordinator,
					st.LogisticsCoordinator,
					st.AccountNumber,
					st.TripStatus,
					st.TravelStatus,
					st.PrimaryDoctor,
					st.Country,
					st.PrimaryTripID,
					st.SPGVRecordId
			FROM SPDSDEV.dbo.SPFT_WMMServiceTrips st
			INNER JOIN dbo.wmm_vintage_to_spft spft
				ON spft.donor_SPFTrecordID = st.RecordId
			
			

		   --we only want to add audit records if there were records inserted
			IF EXISTS (SELECT 1 FROM @inserted_spft) 
				--track the insert records
				INSERT INTO SPDSDEV.DBO.X30_AUDITINFORMATION
					( TABLEID ,
					  TABLERECORDID ,
					  ACTION ,
					  ACTIONDATETIME ,
					  SESSIONRECORDID
					)
				 SELECT 'SPFT_', -- TABLEID - VARCHAR(10)
						recordid, -- TABLERECORDID - BIGINT
						'INSERT', -- ACTION - VARCHAR(10)
						GETDATE(), -- ACTIONDATETIME - DATETIME
						@sessionID  -- SESSIONRECORDID - BIGINT
				 FROM @inserted_spft	
				 
				 
				 INSERT INTO dbo.wmm_service_trip_imports
				 (
				     SPFT_RecordID
				 )
				 SELECT spft.recordid 
				 FROM @inserted_spft spft 
			 
			


			--here we insert the new ACRM record ID so that we can then update the child records
			--record ids to match with the new vintage record ids
			IF EXISTS (SELECT 1 FROM @inserted_spft) 
			BEGIN  

			
			        --update the spft staging table with the inserted SPFT record ID
					UPDATE spft
					SET spft.donor_SPFTrecordID = st.RecordId
					FROM dbo.wmm_vintage_to_spft spft
					INNER JOIN SPDSDEV.dbo.SPFT_WMMServiceTrips st
					ON ISNULL(st.SpecialtyCode,'') = ISNULL(spft.donor_SpecialityCode,'')
					AND ISNULL(st.TripStatus,'') = ISNULL(spft.donor_TripStatus,'')
					AND ISNULL(st.TravelStatus,'') = ISNULL(spft.donor_TravelStatus,'')
					AND ISNULL(st.AccountNumber,'') = ISNULL(spft.donor_AccountNumber,'')
					AND ISNULL(st.PrimaryDoctor,'') = ISNULL(spft.donor_PrimaryDoctorID,'')
					AND ISNULL(st.SPGVRecordId,'') = ISNULL(spft.donor_SPGVRecordID,'')
					AND ISNULL(st.LogisticsCoordinator,'') = ISNULL(spft.donor_LogisticsCoordinator,'')
					AND ISNULL(st.PlacementCoordinator,'') = ISNULL(spft.donor_PlacementCoordinator,'')
					AND ISNULL(st.Country,'') = ISNULL(spft.donor_Country,'')
					
					

					--update the spfu staging table with the inserted SPFT record ID
					UPDATE ur
					SET ur.spft_record_ID = fr.donor_SPFTrecordID
					FROM dbo.wmm_vintage_to_spfu ur
					INNER JOIN dbo.wmm_vintage_to_spft fr
						ON fr.vintage_record_id = ur.Vintage_Record_ID


					--update the spfw staging table with the inserted SPFT record ID
					UPDATE ur
					SET ur.SPFT_RecordID = fr.donor_SPFTrecordID
					FROM dbo.wmm_vintage_to_spfw ur
					INNER JOIN dbo.wmm_vintage_to_spft fr
						ON fr.vintage_record_id = ur.Vintage_RecordID

					--update the spfv staging table with the inserted SPFT record ID
					UPDATE ur
					SET ur.SPFT_RecordID = fr.donor_SPFTrecordID
					FROM dbo.wmm_vintage_to_spfv ur
					INNER JOIN dbo.wmm_vintage_to_spft fr
						ON fr.vintage_record_id = ur.Vintage_RecordID

				    --update the sphs staging table with the inserted SPFT record ID
					UPDATE ur
					SET ur.SPFT_RecordID = fr.donor_SPFTrecordID
					FROM dbo.wmm_vintage_to_sphs ur
					INNER JOIN dbo.wmm_vintage_to_spft fr
						ON fr.vintage_record_id = ur.Vintage_RecordID

					
					--we want to also update the primary trip ID once we have spft record ids
					UPDATE entity
					SET entity.PrimaryTripId = st.donor_PrimaryTripID
					FROM SPDSDEV.dbo.SPFT_WMMServiceTrips entity
					INNER JOIN dbo.wmm_vintage_to_spft st
						ON st.donor_SPFTrecordID = entity.RecordId
					
					--if a trip id is still null after above update then we just set it to its own record id
					UPDATE entity
					SET entity.PrimaryTripId = entity.RecordId
					FROM SPDSDEV.dbo.SPFT_WMMServiceTrips entity
					WHERE entity.PrimaryTripId IS NULL

			END 
			
			
			 --we have made it to the end and so commit the transaction
			-- IF @@TRANCOUNT > 0
			 -- COMMIT TRANSACTION @trans_wmm_vintage_record_insert
		    

END TRY

	BEGIN CATCH
		
		SET @status = 1
		SET @errormesssage = char(13) + @errormesssage + 'Error in  = ' + ERROR_PROCEDURE() + ', procedure line ' + 
									   RTRIM(CONVERT(CHAR(10), ERROR_LINE())) + ', ' +
									   ERROR_MESSAGE() + char(13) +
									   'Transaction : ' + @trans_wmm_vintage_record_insert
	END CATCH
           


/*
  ****************************************************
		END INSERT INTO SPFT TABLE.....
  ****************************************************
*/

/*
	****************************************************
	   INSERT INTO SPFU TABLE.....
	   We need to insert into the Trip Service Times  Table (SPFU)
	   We need to get the record id inserted for this to update the 
	   other child entitiy tables with the correct spft record id
   ****************************************************
*/

BEGIN TRY
	
		--BEGIN TRANSACTION @trans_vintage_spfu_insert

			DECLARE @inserted_spfu TABLE
			(
				recordid BIGINT
			);

			WITH import_into_spfu (	TravelDays,TravelStart,
									TravelEnd, ServiceStart,
									ServiceEnd, spft_record_id,
									ArrivalAtHospital,DepartureAtHospital
									)
			AS
			(

				SELECT	spfu.TravelDays,
						spfu.TravelStart,
						spfu.TravelEnd,
						spfu.ServiceStart,
						spfu.ServiceEnd,
						spfu.spft_record_ID,
						spfu.ArrivalAtHospital,
						spfu.DepartureAtHospital					  
				 FROM dbo.wmm_vintage_to_spfu spfu
				 GROUP BY	spfu.TravelDays,
							spfu.TravelStart,
							spfu.TravelEnd,
							spfu.ServiceStart,
							spfu.ServiceEnd,
							spfu.spft_record_ID,
							spfu.ArrivalAtHospital,
							spfu.DepartureAtHospital
				
			)
			INSERT INTO SPDSDEV.dbo.SPFU_WMMServiceTripDates
			(
			    TravelDays,
			    TravelStart,
			    TravelEnd,
			    ServiceStart,
			    ServiceEnd,
			    SPFTRecordId,
			    ArrivalAtHospital,
			    DepartureFromHospital
			)			
			OUTPUT inserted.RecordId
			INTO @inserted_spfu	
			SELECT i_spfu.TravelDays,
			          i_spfu.TravelStart,
					  i_spfu.TravelEnd,
					  i_spfu.ServiceStart,
					  i_spfu.ServiceEnd,
					   i_spfu.spft_record_id,
					  i_spfu.ArrivalAtHospital,
					  i_spfu.DepartureAtHospital					 
			FROM import_into_spfu i_spfu
			EXCEPT 
			SELECT	sd.TravelDays,
					sd.TravelStart,
					sd.TravelEnd,
					sd.ServiceStart,
					sd.ServiceEnd,
					sd.SPFTRecordId,
					sd.ArrivalAtHospital,
					sd.DepartureFromHospital					
			FROM SPDSDEV.dbo.SPFU_WMMServiceTripDates sd			
			

		   --we only want to add audit records if there were records inserted
			IF EXISTS (SELECT 1 FROM @inserted_spfu) 
				--track the insert records
				INSERT INTO SPDSDEV.DBO.X30_AUDITINFORMATION
					( TABLEID ,
					  TABLERECORDID ,
					  ACTION ,
					  ACTIONDATETIME ,
					  SESSIONRECORDID
					)
				 SELECT 'SPFU_', -- TABLEID - VARCHAR(10)
						recordid, -- TABLERECORDID - BIGINT
						'INSERT', -- ACTION - VARCHAR(10)
						GETDATE(), -- ACTIONDATETIME - DATETIME
						@sessionID  -- SESSIONRECORDID - BIGINT
				 FROM @inserted_spfu			 

			 --we have made it to the end and so commit the transaction
			-- IF @@TRANCOUNT > 0
			 -- COMMIT TRANSACTION @trans_vintage_spfu_insert
		    

END TRY

	BEGIN CATCH
		
		SET @status = 1
		SET @errormesssage = char(13) + @errormesssage + 'Error in  = ' + ERROR_PROCEDURE() + ', procedure line ' + 
									   RTRIM(CONVERT(CHAR(10), ERROR_LINE())) + ', ' +
									    ERROR_MESSAGE() --+ char(13) +
									   -- 'Transaction : ' + @trans_vintage_spfu_insert
	END CATCH
           


/*
  ****************************************************
		END INSERT INTO SPFU TABLE.....
  ****************************************************
*/


/*
	****************************************************
	   INSERT INTO SPFV TABLE.....
	   Second, we need to insert into the primary Service Trip Details Table (SPFV)
	   The Parent table (SPFT) record id is already in the staging table from udpates
	   above in the Insert into SPFT table block (towards end you will see updates)
   ****************************************************
*/

BEGIN TRY
	
		--BEGIN TRANSACTION @trans_vintage_spfv_insert

			DECLARE @inserted_spfv TABLE
			(
				recordid BIGINT
			);


			WITH import_into_spfv (		DailyRate, SPFTRecordID, 
										SecondarySpeciality, ProjectOf,
										NumberofChildren,Comments,PAACS,
										ResidentFunding)
			AS
			(
				SELECT CONVERT(MONEY, spfv.DailyRate),
					   spfv.SPFT_RecordID,
					   spfv.donor_SecondarySpeciality,
					   spfv.ProjectOf,
					   spfv.NumberofChildren,
					   spfv.Comments,
					   spfv.PAACS,
					   spfv.ResidentFunding
				FROM dbo.wmm_vintage_to_spfv spfv
				GROUP BY CONVERT(MONEY, spfv.DailyRate),
						   spfv.SPFT_RecordID,
						   spfv.donor_SecondarySpeciality,
						   spfv.ProjectOf,
						   spfv.NumberofChildren,
						   spfv.Comments,
						   spfv.PAACS,
						   spfv.ResidentFunding
			)
			INSERT INTO SPDSDEV.dbo.SPFV_WMMServiceTripDetails
			(
				DailyRate,
				SPFTRecordId,
				SecondarySpecialty,
				ProjectOf,
				NumberOfChildren,
				Comments,
				PAACS,
				ResidentFunding
			)
			OUTPUT inserted.RecordId
			INTO @inserted_spfv	
			SELECT ir.DailyRate,
					ir.SPFTRecordID,
					ir.SecondarySpeciality,
					ir.ProjectOf,
					ir.NumberofChildren,
					ir.Comments,
					ir.PAACS,
					ir.ResidentFunding
			FROM import_into_spfv ir
			EXCEPT 
			SELECT	td.DailyRate,
					td.SPFTRecordId,
					td.SecondarySpecialty,
					td.ProjectOf,
					td.NumberOfChildren,
					td.Comments,
					td.PAACS,
					td.ResidentFunding
			FROM SPDSDEV.dbo.SPFV_WMMServiceTripDetails td

			--we only want to add audit records if there were records inserted
			IF EXISTS (SELECT 1 FROM @inserted_spfv) 
				--track the insert records
				INSERT INTO SPDSDEV.DBO.X30_AUDITINFORMATION
					( TABLEID ,
					  TABLERECORDID ,
					  ACTION ,
					  ACTIONDATETIME ,
					  SESSIONRECORDID
					)
				 SELECT 'SPFV_', -- TABLEID - VARCHAR(10)
						recordid, -- TABLERECORDID - BIGINT
						'INSERT', -- ACTION - VARCHAR(10)
						GETDATE(), -- ACTIONDATETIME - DATETIME
						@sessionID  -- SESSIONRECORDID - BIGINT
				 FROM @inserted_spfv	


				  --we have made it to the end and so commit the transaction
			-- IF @@TRANCOUNT > 0
			 -- COMMIT TRANSACTION @trans_vintage_spfv_insert

END TRY

BEGIN CATCH
		
	SET @status = 1
	SET @errormesssage = char(13) + @errormesssage + 'Error in  = ' + ERROR_PROCEDURE() + ', procedure line ' + 
									RTRIM(CONVERT(CHAR(10), ERROR_LINE())) + ', ' +
									ERROR_MESSAGE() --+ char(13) +
									--'Transaction : ' + @trans_vintage_spfv_insert
END CATCH

/*
	****************************************************
	   
	   END INSERT INTO SPFV TABLE.....
	   
   ****************************************************
*/

/*
	****************************************************
	   INSERT INTO SPFW TABLE (Travel Logistics).....
	   Third, we need to insert into the Trip logistics Table (SPFW)
	   The Parent table (SPFT) record id is already in the staging table from udpates
	   above in the Insert into SPFT table block (towards end you will see updates)
   ****************************************************
*/

BEGIN TRY
	
		--BEGIN TRANSACTION @trans_vintage_spfw_insert

			DECLARE @inserted_spfw TABLE
			(
				recordid BIGINT
			);

			
			WITH import_into_spfw (WMMFlightAssistance, DoctorPurchasedFlights, 
								   BibleSent, NameForBible,MedicalDocumentsSent, 
								   HospitalConfirmedDocuments,SPFTRecordID)
			AS
			(
				SELECT	spfw.WMMFlightAssistance, 
						spfw.DoctorPurchasedFlights, 
						spfw.BibleSent, 
						spfw.NameForBible,
						spfw.MedicalDocumentsSent, 
						spfw.HospitalConfirmedDocuments,
						spfw.SPFT_RecordID 
				FROM dbo.wmm_vintage_to_spfw spfw
				GROUP BY spfw.WMMFlightAssistance, 
						spfw.DoctorPurchasedFlights, 
						spfw.BibleSent, 
						spfw.NameForBible,
						spfw.MedicalDocumentsSent, 
						spfw.HospitalConfirmedDocuments,
						spfw.SPFT_RecordID 
			)
			INSERT INTO SPDSDEV.dbo.SPFW_WMMServiceTripLogistics
			(
			    WMMFlightAssistance,
			    DoctorPurchasedFlights,
			    BibleSent,
			    NameForBible,
			    MedicalDocumentsSent,
			    HospitalConfirmedDocuments,
			    SPFTRecordId
			)
			OUTPUT inserted.RecordId
			INTO @inserted_spfw
            SELECT	spfw.WMMFlightAssistance, 
						spfw.DoctorPurchasedFlights, 
						spfw.BibleSent, 
						spfw.NameForBible,
						spfw.MedicalDocumentsSent, 
						spfw.HospitalConfirmedDocuments,
						spfw.SPFTRecordID
		   FROM import_into_spfw spfw
		   EXCEPT 
		   SELECT	tl.WMMFlightAssistance,
					tl.DoctorPurchasedFlights,
					tl.BibleSent,
					tl.NameForBible,
					tl.MedicalDocumentsSent,
					tl.HospitalConfirmedDocuments,
					tl.SPFTRecordId
			FROM SPDSDEV.dbo.SPFW_WMMServiceTripLogistics tl

		   --we only want to add audit records if there were records inserted
			IF EXISTS (SELECT 1 FROM @inserted_spfw) 
				--track the insert records
				INSERT INTO SPDSDEV.DBO.X30_AUDITINFORMATION
					( TABLEID ,
					  TABLERECORDID ,
					  ACTION ,
					  ACTIONDATETIME ,
					  SESSIONRECORDID
					)
				 SELECT 'SPFW_', -- TABLEID - VARCHAR(10)
						recordid, -- TABLERECORDID - BIGINT
						'INSERT', -- ACTION - VARCHAR(10)
						GETDATE(), -- ACTIONDATETIME - DATETIME
						@sessionID  -- SESSIONRECORDID - BIGINT
				 FROM @inserted_spfw
		
		
		-- IF @@TRANCOUNT > 0
			 -- COMMIT TRANSACTION @trans_vintage_spfw_insert


END TRY

BEGIN CATCH
		
	SET @status = 1
	SET @errormesssage = char(13) + @errormesssage + 'Error in  = ' + ERROR_PROCEDURE() + ', procedure line ' + 
									RTRIM(CONVERT(CHAR(10), ERROR_LINE())) + ', ' +
									ERROR_MESSAGE() --+ char(13) +
									--'Transaction : ' + @trans_vintage_spfw_insert
END CATCH

/*
	****************************************************
	   
	   END INSERT INTO SPFW TABLE.....
	   
   ****************************************************
*/

/*
	****************************************************
	   INSERT INTO SPHS TABLE (Post Trip details).....
	   We need to insert into the Post Trip Table (SPHS)
   ****************************************************
*/

BEGIN TRY
	
		--BEGIN TRANSACTION @trans_vintage_sphs_insert

			DECLARE @inserted_sphs TABLE
			(
				recordid BIGINT
			);

			
			WITH import_into_sphs (	SPFT_RecordID,Received,
									 Report_Date, URL, 
									 ServiceReceived, ServiceDate,
									 ServiceURL, MinistryReceived,
									 MinistryDate,MinistryURL, 
									 ExpenseReceived,ExpenseDate,
									  ExpenseURL)
			AS
			(
				SELECT	sphs.SPFT_RecordID,
						sphs.Received,
						sphs.Report_Date,
						sphs.URL,
						sphs.ServiceReceived,
						sphs.ServiceDate,
						sphs.ServiceURL,
						sphs.MinistryReceived,
						sphs.MinistryDate,
						sphs.MinistryURL,
						sphs.ExpenseReceived,
						sphs.ExpenseDate,
						sphs.ExpenseURL
				FROM dbo.wmm_vintage_to_sphs sphs
				GROUP BY sphs.SPFT_RecordID,
						sphs.Received,
						sphs.Report_Date,
						sphs.URL,
						sphs.ServiceReceived,
						sphs.ServiceDate,
						sphs.ServiceURL,
						sphs.MinistryReceived,
						sphs.MinistryDate,
						sphs.MinistryURL,
						sphs.ExpenseReceived,
						sphs.ExpenseDate,
						sphs.ExpenseURL
			)
			INSERT INTO SPDSDEV.dbo.SPHS_WMMPostTrip
			(
			    SPFTRecordId,
			    Received,
			    Date,
			    URL,
			    ServiceReceived,
			    ServiceDate,
			    ServiceURL,
			    MinistryReceived,
			    MinistryDate,
			    MinistryURL,
			    ExpenseReceived,
			    ExpenseDate,
			    ExpenseURL
			)			
			OUTPUT inserted.RecordId
			INTO @inserted_sphs
            SELECT		sphs.SPFT_RecordID,
						sphs.Received,
						sphs.Report_Date,
						sphs.URL,
						sphs.ServiceReceived,
						sphs.ServiceDate,
						sphs.ServiceURL,
						sphs.MinistryReceived,
						sphs.MinistryDate,
						sphs.MinistryURL,
						sphs.ExpenseReceived,
						sphs.ExpenseDate,
						sphs.ExpenseURL
		   FROM import_into_sphs sphs
		   EXCEPT 
		   SELECT	pt.SPFTRecordId,
					pt.Received,
					pt.Date,
					pt.URL,
					pt.ServiceReceived,
					pt.ServiceDate,
					pt.ServiceURL,
					pt.MinistryReceived,
					pt.MinistryDate,
					pt.MinistryURL,
					pt.ExpenseReceived,
					pt.ExpenseDate,
					pt.ExpenseURL
			FROM SPDSDEV.dbo.SPHS_WMMPostTrip pt

		   --we only want to add audit records if there were records inserted
			IF EXISTS (SELECT 1 FROM @inserted_sphs) 
				--track the insert records
				INSERT INTO SPDSDEV.DBO.X30_AUDITINFORMATION
					( TABLEID ,
					  TABLERECORDID ,
					  ACTION ,
					  ACTIONDATETIME ,
					  SESSIONRECORDID
					)
				 SELECT 'SPHS_', -- TABLEID - VARCHAR(10)
						recordid, -- TABLERECORDID - BIGINT
						'INSERT', -- ACTION - VARCHAR(10)
						GETDATE(), -- ACTIONDATETIME - DATETIME
						@sessionID  -- SESSIONRECORDID - BIGINT
				 FROM @inserted_sphs
		
		
		-- IF @@TRANCOUNT > 0
			 -- COMMIT TRANSACTION @trans_vintage_sphs_insert


END TRY

BEGIN CATCH
		
	SET @status = 1
	SET @errormesssage = char(13) + @errormesssage + 'Error in  = ' + ERROR_PROCEDURE() + ', procedure line ' + 
									RTRIM(CONVERT(CHAR(10), ERROR_LINE())) + ', ' +
									ERROR_MESSAGE() --+ char(13) +
									--'Transaction : ' + @trans_vintage_sphs_insert
END CATCH

/*
	****************************************************
	   
	   END INSERT INTO SPHS TABLE.....
	   
   ****************************************************
*/













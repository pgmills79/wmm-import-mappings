INSERT INTO SPDSDEV.dbo.SPGV_WMMHospitals
	(
	    AccountNumber
	)
	SELECT h.AccountNumber 
	FROM dbo.wmm_active_hospitals h
	WHERE NOT EXISTS (SELECT * FROM SPDSDEV.dbo.SPGV_WMMHospitals h2
						WHERE h2.AccountNumber = h.AccountNumber)
	EXCEPT
	SELECT ih.AccountNumber 
	FROM SPDSDEV.dbo.SPGV_WMMHospitals ih
--service trip import
--tablke name in spapps: wmm_vintage_to_spft
SELECT  NULL AS donor_SPFTrecordID,
		st.new_specialtycodeidName AS vintage_SpecialityCode, --nvarchar255)
	    NULL AS donor_SpecialityCode, --bigint
		st.OwnerIdName AS vintage_PlacementCoordinator, --ncarchar(160)
		NULL AS donor_PlacementCoordinator, --bigint
		st.new_secondaryuseridName AS vintage_LogisticsCoordinator, --nvarchar(200)
		NULL  AS donor_LogisticsCoordinator, --bigint
		a.AccountNumber  AS vintage_AccountNumber, --nvarchar_20
		CONVERT(BIGINT, a.AccountNumber) AS donor_AccountNumber, --bigint  
		st.New_TripStatus AS vintage_TripStatus, --int
		CASE WHEN st.New_TripStatus = 1 THEN 'A'  --Active status which is 'A' in Donor (advanced ACRM)
		WHEN st.New_TripStatus = 2 THEN 'C'  --Cancelled status which is 'C' in Donor (advanced ACRM)
		ELSE ''
		END AS donor_TripStatus, --varchar(10)
		st.New_TravelStatus AS vintage_TravelStatus,
		CASE WHEN st.New_TravelStatus = 1 THEN 'T' --Tentative status which is 'T' in Donor (advanced ACRM)
		WHEN st.New_TravelStatus = 2 THEN 'F' --Final status which is 'F' in Donor (advanced ACRM)
		WHEN st.New_TravelStatus = 3 THEN 'F' --Cancelled status which is 'F' in Donor (advanced ACRM)
		WHEN st.New_TravelStatus = 4 THEN 'P' --Planning(PLacement Cord) status which is 'P' in Donor (advanced ACRM)
		ELSE ''
		END AS donor_TravelStatus,
		st.new_primarydoctoridName AS vintage_PrimaryDoctorName,
		NULL AS donor_PrimaryDoctorName,
		st.new_primarydoctorid as vintage_PrimaryDoctorID,
		NULL AS donor_PrimaryDoctorID, --bigint,
		 st.New_Country AS vintage_Country, --nvarchar(100)
		 UPPER(st.New_Country) AS donor_Country, --varchar(10),
		 st.new_primarytriprecordid AS vintage_PrimaryTripID,
		 NULL AS donor_PrimaryTripID, --bigint
		 a.AccountNumber AS vintage_SPGVRecordID,
		 NULL AS donor_SPGVRecordID, --bigint
		 st.New_servicetripId AS vintage_record_id
FROM dbo.New_servicetrip st
LEFT JOIN dbo.Account a
ON a.AccountId = st.new_account


--service dates
SELECT  st.New_servi AS Vintage_Record_ID,
		 st.New_TravelDays AS TravelDays,
		CONVERT(VARCHAR(10),st.New_TravelDateStart, 126) AS TravelStart,
		CONVERT(VARCHAR(10),st.New_TravelDateEnd, 126) AS TravelEnd,
		CONVERT(VARCHAR(10),st.New_ServiceDateStart, 126) AS ServiceStart,
		CONVERT(VARCHAR(10),st.New_ServiceDateEnd, 126) AS ServiceEnd,
		NULL AS spft_record_ID,
		CONVERT(VARCHAR(10),st.new_ArrivalatHospital, 126) AS ArrivalAtHospital,
		CONVERT(VARCHAR(10),st.new_DeparturefromHospital, 126) AS DepartureAtHospital
 FROM dbo.New_servicetrip st


 --post trip import
 SELECT NULL AS SPFT_RecordID,  --will get filled later (bigint)
		ISNULL(st.New_ReceivedTripReport,0) AS Received,  --bit in donor
		st.New_ReceivedTripReportDate AS Report_Date, --this is datetime in donor
		REPLACE(st.New_TripReportURL,'NULL',NULL) AS URL,  --convert to varchar(max)
		ISNULL(st.New_ReceivedServiceEvaluation,0) AS ServiceReceived, --convert to bit
		st.New_ReceivedServiceEvaluationDate AS ServiceDate, -- convert to datetime
		REPLACE(st.New_ServiceEvaluationURL,'NULL',NULL) AS ServiceURL, --varcharchar(max)
		ISNULL(st.New_ReceivedMinistryEvaluation,0) AS MinistryReceived, --bit in Donor
		st.New_ReceivedMinistryEvaluationDate AS MinistryDate, --datetime
		REPLACE(st.New_MinistryEvaluationURL, 'NULL',NULL) AS MinistryURL, --varchar(max)
		ISNULL(st.New_ReceivedExpenseEnvelope,0) AS ExpenseReceived, --bit in donor
		st.New_ReceivedExpenseEnvelopeDate AS ExpenseDate, --datetime
		REPLACE(st.New_ExpenseEnvelopeURL,'NULL',NULL) AS ExpenseURL --varchar(max) in donor
  FROM dbo.New_servicetrip st
  
  
  
--then update the stage table in Donor

--for some reason the vintage record ids come across with brackets so we need to remove the brackets
UPDATE spft
SET spft.vintage_record_id = REPLACE(REPLACE(spft.vintage_record_id, '{',''),'}','')
FROM dbo.wmm_vintage_to_spft spft

--update donor SPGV Record ID
UPDATE spft
SET spft.donor_SPGVRecordID = spft.vintage_SPGVRecordID
FROM dbo.wmm_vintage_to_spft spft

--update speciality codes for Donor Import
UPDATE spft
SET spft.donor_SpecialityCode = c.ACRMSpecialtyCode 
FROM dbo.wmm_vintage_to_spft spft
INNER JOIN SPAPPSDEV.dbo.WMMCRMToACRMCodes c
	ON UPPER(RTRIM(LTRIM(c.VintageSpecialty))) = RTRIM(UPPER(spft.vintage_SpecialityCode))
	
--update placement cordinator column
UPDATE spft
SET spft.donor_PlacementCoordinator = ISNULL(
						(SELECT a.AccessorId
							FROM DSSECDEV.dbo.SEC00_InternalUsers iu
								INNER JOIN dbo.wmm_vintage_to_spft vs
								ON LOWER(LTRIM(RTRIM(spft.vintage_PlacementCoordinator))) = LOWER(RTRIM(LTRIM(iu.Description)))
								INNER JOIN DSSECDEV.dbo.SEC03_Accessors a
								ON a.SourceId = iu.UserId
							WHERE  LOWER(RTRIM(LTRIM(iu.Description))) = LOWER(RTRIM(LTRIM(spft.vintage_PlacementCoordinator)))
							GROUP BY	a.AccessorId)
					    ,
						(
							SELECT TOP 1 a.AccessorId 
							FROM DSSECDEV.dbo.SEC03_Accessors a
							WHERE LOWER(a.SourceId) = 'wmm'
						)
						)
FROM dbo.wmm_vintage_to_spft spft

--update logistics cordinator codes
UPDATE ur
SET ur.donor_LogisticsCoordinator = ISNULL(
						(SELECT a.AccessorId
							FROM DSSECDEV.dbo.SEC00_InternalUsers iu
								INNER JOIN dbo.wmm_vintage_to_spft vs
								ON LOWER(LTRIM(RTRIM(ur.vintage_LogisticsCoordinator))) = LOWER(RTRIM(LTRIM(iu.Description)))
								INNER JOIN DSSECDEV.dbo.SEC03_Accessors a
								ON a.SourceId = iu.UserId
							WHERE  LOWER(RTRIM(LTRIM(iu.Description))) = LOWER(RTRIM(LTRIM(ur.vintage_LogisticsCoordinator)))
							GROUP BY	a.AccessorId)
					    ,
						(
							SELECT TOP 1 a.AccessorId 
							FROM DSSECDEV.dbo.SEC03_Accessors a
							WHERE LOWER(SourceId) = 'wmm'
						)
						)
FROM dbo.wmm_vintage_to_spft ur
DELETE 
FROM SPDSDEV.dbo.SPHS_WMMPostTrip 
WHERE SPFTRecordId IN (SELECT SPFT_RecordID FROM dbo.wmm_service_trip_imports)

DELETE 
FROM SPDSDEV.dbo.SPFW_WMMServiceTripLogistics
WHERE SPFTRecordId IN (SELECT SPFT_RecordID FROM dbo.wmm_service_trip_imports)

DELETE 
FROM SPDSDEV.dbo.SPFV_WMMServiceTripDetails 
WHERE SPFTRecordId IN (SELECT SPFT_RecordID FROM dbo.wmm_service_trip_imports)


DELETE FROM SPDSDEV.dbo.SPFU_WMMServiceTripDates 
WHERE SPFTRecordId IN (SELECT SPFT_RecordID FROM dbo.wmm_service_trip_imports)

DELETE FROM SPDSDEV.dbo.SPFT_WMMServiceTrips 
WHERE RecordId IN (SELECT SPFT_RecordID FROM dbo.wmm_service_trip_imports)

--then delete the test import records
DELETE FROM dbo.wmm_service_trip_imports

--reset all the spft record ids to null
UPDATE spft
SET spft.SPFT_RecordID = NULL 
FROM  dbo.wmm_vintage_to_spft spft

--reset all the spft record ids to null
UPDATE spfu
SET spfu.spft_record_ID = NULL 
FROM  dbo.wmm_vintage_to_spfu spfu

--reset all the spft record ids to null
UPDATE spfv
SET spfv.SPFT_RecordID = NULL 
FROM  dbo.wmm_vintage_to_spfv spfv

--reset all the spft record ids to null
UPDATE spfw
SET spfw.SPFT_RecordID = NULL 
FROM  dbo.wmm_vintage_to_spfw spfw

--reset all the spft record ids to null
UPDATE spfw
SET spfw.SPFT_RecordID = NULL 
FROM  dbo.wmm_vintage_to_sphs spfw


--SELECT COUNT(*) FROM SPDSDEV.dbo.SPFT_WMMServiceTrips   --count:9,624
--SELECT COUNT(*) FROM SPDSDEV.dbo.SPFU_WMMServiceTripDates --count:24733
--SELECT COUNT(*) FROM SPDSDEV.dbo.SPFV_WMMServiceTripDetails --count:23969
--SELECT COUNT(*) FROM SPDSDEV.dbo.SPFW_WMMServiceTripLogistics --count: 10736
--SELECT COUNT(*) FROM SPDSDEV.dbo.SPHS_WMMPostTrip --count: 10846

--SELECT * FROM dbo.wmm_vintage_to_spft WHERE SPFT_RecordID = 219934

--SELECT COUNT(*) FROM dbo.wmm_vintage_to_spft WHERE SPFT_RecordID IS NULL --count: 21
--SELECT COUNT(*) FROM dbo.wmm_vintage_to_spfu WHERE spft_record_ID IS NULL --count: 28
--SELECT COUNT(*) FROM dbo.wmm_vintage_to_spfv WHERE SPFT_RecordID IS NULL --count: 21
--SELECT COUNT(*) FROM dbo.wmm_vintage_to_spfw WHERE SPFT_RecordID IS NULL --count: 21


--SELECT spft.SPFT_RecordID, COUNT(*) AS num_times 
--FROM dbo.wmm_vintage_to_spft spft
--GROUP BY spft.SPFT_RecordID
--HAVING COUNT(*) > 1


--SELECT * FROM dbo.wmm_vintage_to_spft WHERE SPFT_RecordID = 219934

--update placement cordinator codes
UPDATE ur
SET ur.import_placement_cordinator = ISNULL(
						(SELECT a.AccessorId
							FROM DSSECDEV.dbo.SEC00_InternalUsers iu
								INNER JOIN dbo.wmm_vintage_to_spft vs
								ON LOWER(LTRIM(RTRIM(ur.PlacementCoordinator))) = LOWER(RTRIM(LTRIM(iu.Description)))
								INNER JOIN DSSECDEV.dbo.SEC03_Accessors a
								ON a.SourceId = iu.UserId
							WHERE  LOWER(RTRIM(LTRIM(iu.Description))) = LOWER(RTRIM(LTRIM(ur.PlacementCoordinator)))
							GROUP BY	a.AccessorId)
					    ,
						(
							SELECT TOP 1 a.AccessorId 
							FROM DSSECDEV.dbo.SEC03_Accessors a
							WHERE LOWER(SourceId) = 'wmm'
						)
						)
FROM dbo.wmm_vintage_to_spft ur



--update logistics cordinator codes
UPDATE ur
SET ur.import_logistics_cordinator = ISNULL(
						(SELECT a.AccessorId
							FROM DSSECDEV.dbo.SEC00_InternalUsers iu
								INNER JOIN dbo.wmm_vintage_to_spft vs
								ON LOWER(LTRIM(RTRIM(ur.LogisticsCoordinator))) = LOWER(RTRIM(LTRIM(iu.Description)))
								INNER JOIN DSSECDEV.dbo.SEC03_Accessors a
								ON a.SourceId = iu.UserId
							WHERE  LOWER(RTRIM(LTRIM(iu.Description))) = LOWER(RTRIM(LTRIM(ur.LogisticsCoordinator)))
							GROUP BY	a.AccessorId)
					    ,
						(
							SELECT TOP 1 a.AccessorId 
							FROM DSSECDEV.dbo.SEC03_Accessors a
							WHERE LOWER(SourceId) = 'wmm'
						)
						)
FROM dbo.wmm_vintage_to_spft ur

--update trip status and travel status codes
UPDATE ur
SET ur.import_trip_status = CASE WHEN ur.TripStatus = 1 THEN 'A'  --Active status which is 'A' in Donor (advanced ACRM)
							WHEN ur.TripStatus = 2 THEN 'C'  --Cancelled status which is 'C' in Donor (advanced ACRM)
							ELSE ''
							END,
	ur.import_travel_status = CASE WHEN ur.TravelStatus = 1 THEN 'T' --Tentative status which is 'T' in Donor (advanced ACRM)
								 WHEN ur.TravelStatus = 2 THEN 'F' --Final status which is 'F' in Donor (advanced ACRM)
								 WHEN ur.TravelStatus = 3 THEN 'F' --Cancelled status which is 'F' in Donor (advanced ACRM)
								 WHEN ur.TravelStatus = 4 THEN 'P' --Planning(PLacement Cord) status which is 'P' in Donor (advanced ACRM)
								 ELSE ''
								END
FROM dbo.wmm_vintage_to_spft ur

--replace import trip ids
UPDATE ur
SET ur.import_primary_trip_ID = CONVERT(BIGINT, REPLACE(ur.PrimaryTripID, 'NULL', NULL))
FROM dbo.wmm_vintage_to_spft ur

--update hospital records
UPDATE r
	SET r.import_SPGV_RecordID = h.RecordId
FROM dbo.wmm_vintage_to_spft  r
INNER JOIN SPDSDEV.dbo.SPGV_WMMHospitals h
	ON h.AccountNumber = r.hospital_account_number_SPGV
WHERE r.import_SPGV_RecordID IS NULL 

--update primary doctor
UPDATE ur
SET ur.PrimaryDoctor = NULL 
FROM dbo.wmm_vintage_to_spft ur
WHERE ur.PrimaryDoctor = 'NULL'
--update primary doctor
UPDATE ur
SET ur.import_primary_doctor = CONVERT(BIGINT, REPLACE(ur.PrimaryDoctor, 'NULL', NULL))
FROM dbo.wmm_vintage_to_spft ur
WHERE ur.import_primary_doctor IS NULL 
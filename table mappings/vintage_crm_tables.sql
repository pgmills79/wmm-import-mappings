

--hospitals
SELECT * --AccountNumber AS account_number 
FROM dbo.Account

--doctors
SELECT New_AccountNumber AS account_number 
FROM dbo.Contact
WHERE New_AccountNumber IS  NOT NULL



SELECT acc.* 
FROM dbo.Account acc
INNER JOIN dbo.New_servicetrip nst
	ON nst.


	/*
	**** **TABLE*** New_servicetrip   ****TABLE***
	**** Field Mappings (DB column name -> Widow Field Name) Window in Vintage CRM (Service Trip):
	****
	**** new_DeparturefromHospital -> Departure from Hospital
	**** new_ArrivalatHospital -> Arrival at Hospital
	**** New_totaldays -> Complete Days
	**** New_secondaryuserid -> Complete Days
	**** New_ServiceValue -> Service Value
	**** New_ProjectOf -> gives code value of the project
	**** New_OverrideDailyRate -> Ovverride Daily Rate
	**** New_DailyRate -> Daily Rate
	**** New_TravelStatus -> (code from another table) this example shows 2
	**** New_TripStatus -> (code from another table) this example shows 1
	**** new_contactidName -> doctor/person name
	**** New_Country -> Country
	**** OwnerIdName -> Placement Coordinator
	**** New_name -> Name (of hospital)
	**** new_accountName -> Hospital (this is shorter version of hospital name)
	**** New_Comment -> Comments
	****
	
	
	*/
SELECT  nst.New_Comment
FROM dbo.New_servicetrip nst
WHERE nst.New_TravelDateStart BETWEEN '10/10/2018' AND '10/12/2018'
AND nst.new_accountName = 'AIC-CURE International Children''s Hospital'


/*
	**** **TABLE*** Contact  ****TABLE***
	**** Field Mappings (DB column name -> Widow Field Name) Window in Vintage CRM (Service Trip):
	****
	**** FullName -> Doctor/Person on service trip form
	**** FullName -> Doctor/Person on service trip form
	
	****
	
	
	*/

SELECT c.Address1_PrimaryContactName
FROM dbo.Contact c WHERE c.ContactId = '4D4B6E4B-49E1-DF11-B0F8-001A64324B78'






SELECT DISTINCT(nc.New_D2D_CodeType) FROM dbo.New_Codes  nc WHERE nc.New_D2D_ValueDescription LIKE '%Active%'


SELECT * FROM dbo.StatusMap


/***  Table SPFT_WMMServiceTrips  Table
-- fields

-- SpecialtyCode -- Table: X04_CodeValues 
--					Column: Value 
--					Where: Type = WMMSPECIAL

--				    Vintage CRM Mapping:  SELECT  nst.new_specialtycodeidName
--											FROM dbo.New_servicetrip nst

-- PlacementCoordinator --	Table: SEC03_Accessors and SEC00_InternalUsers from the security database (they are joined by userid and source id 
--							Column: AccessorID/description --							
--				    Vintage CRM Mapping:  Table: New_servicetrip, OwnerIdName

-- LogisticsCoordinator --	Table: SEC03_Accessors and SEC00_InternalUsers from the security database (they are joined by userid and source id 
--							Column: AccessorID/description --							
--				    Vintage CRM Mapping:  Table: New_servicetrip, OwnerIdName

-- AccountNumber --	Table: A01_AccountMaster 
--							Column: AccountNumber --							
--				    Vintage CRM Mapping:  Table: New_servicetrip Column: new_account 
										  join Table: Account on Account.AccountId =  New_servicetrip.new_account

-- Trip Status     --Table: X04_CodeValues 
--					Column: VALUE
					Where: 	Type = 'WMMTRIP' and Active = 1						
--				    Vintage CRM Mapping:  
								Code Values:
								1 = Active (value = A in Donor)
								2 = Cancelled     (value = C in Donor)
										  
-- Travel Status    --Table: X04_CodeValues 
--					Column: VALUE
					Where: 	Type = 'WMMTRAVEL' and Active = 1						
--				    Vintage CRM Mapping:  
								Code Values:
								1 = Tentative (value = T in Donor)
								2 = Final     (value = F in Donor)
								3 = Cancelled (value = F in Donor)
								4 = Planning (Placement Coordinator) (value = P in Donor)

-- Primary Doctor    --Table: A01_AccountMaster 
--					  Column: AccountNumber				
--				    Vintage CRM Mapping: 
						Table:  New_servicetrip
						Column: new_primarydoctorid	
						join Table: Contact Column: Contact.ContactId = New_servicetrip.new_primarydoctorid

-- Country         --	SELECT cv.Value 
						FROM SPDSDEV.dbo.X04_CodeValues cv
						WHERE cv.Type = 'DDCCOUNTRY'			
--				    Vintage CRM Mapping: 
						Table:  New_servicetrip
						Column: New_Country	

-- Primary Trip ID         --	This is the record id of the SPFT_WMMServiceTrips table.
							--a primary trip can have multiple service trips	
--				    Vintage CRM Mapping: 
						Table:  New_servicetrip
						Column: new_primarytriprecordid	

-- SPGVRecordId     Table: SPGV_WMMHospitals
				    Column: Record ID (this ties to the hosptial account number)
--				    Vintage CRM Mapping: 
						Table:  New_servicetrip
						Column: new_account	
						join table: Account on Account.AccountId = New_servicetrip.new_account

****/




SELECT * 
FROM SPDSDEV.dbo.SPFT_WMMServiceTrips

/***  Table SPFW_WMMServiceTripLogistics  Table
-- fields

-- WMMFlightAssistance --  this is a bit (Is WMM Assisting with flights?)
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_logistics_wmmresponsibleforflights
						Values: Yes = 1, No = 2, Unknown = 3  (how will this map for the unknowns?)

-- DoctorPurchasedFlights --  this is a bit (Is WMM Assisting with flights?)
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_logistics_doctorpurchasedflights
						Values: Yes = 1, Not Yet = 2 (how will this map for the not yets?)

-- BibleSent --        this is a bit (Is WMM Assisting with flights?)
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_logistics_BibleSent
						Values: Not Sent = 1, Request Sent to AA; Pending Engraving = 2, 
								Pending Mailing Bible/BGH & Letter = 4 
								Bible Sent = 5 
								
-- BibleSent --        this is a bit (Was a Bible Sent?)
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_logistics_BibleSent
						Values: Not Sent = 1, Request Sent to AA; Pending Engraving = 2, 
								Pending Mailing Bible/BGH & Letter = 4 
								Bible Sent = 5  
								
-- Name for Bible --        Varchar(70)
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_logistics_nameonbible

-- MedicalDocumentSent --        this is a date
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: new_MedicalLicensingDocumentsSenttoHospital

-- HospitalConfirmedDocuments --        this is a date
				--						Vintage CRM Mapping:  
										Table: New_servicetrip
										Column: new_HospitalConformedReceiptofMedicalLicensin

-- SPFTRecordID--						 BIGINT (this is the record id of the primary SPFT table record)
				--						Vintage CRM Mapping:  
										Table: New_servicetrip
										Column: new_HospitalConformedReceiptofMedicalLicensin

****/

SELECT * FROM SPDSDEV.dbo.SPFW_WMMServiceTripLogistics



/***  Table SPFV_WMMServiceTripDetails  Table
-- fields

-- DailyRate --  this is a money value
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_DailyRate

-- SPFTRecordID--						 BIGINT (this is the record id of the primary SPFT table record)
				--						Vintage CRM Mapping:  
										Table: New_servicetrip
										Column: new_HospitalConformedReceiptofMedicalLicensin

-- SecondarySpeciality --  Varchar10
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: new_SecondarySpecialty
						join table: New_specialization on New_specialization.New_specializationId = New_servicetrip.new_SecondarySpecialty

-- ProjectOf --  Varchar10
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_ProjectOf
						Values: 1 = WMM, 2 = SP, 3 = No Trip Report

-- NumberofChildren --  bigint
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_NumberofChildren

-- Comments --  varchar(max)
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_Comment

-- PAACS --  bit  
--						Vintage CRM Mapping (Record Service to PAACS):  
						Table: New_servicetrip
						Column: New_RecordServicetoPAACS
						Values: 1 = Yes, 2 = No

-- ResidentFunding --   varchar(10) -this has the actual text values
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_ResidentFunding
						Values: 1 = PART, 2 = POST, 3 = TOTAL

****/



--where at in old crm
SELECT * FROM SPDSDEV.dbo.SPFV_WMMServiceTripDetails


/***  Table SPFU_WMMServiceTripDates  Table
-- fields

-- Travel Days --   Varchar(70)
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_TravelDays

-- Travel Start --   date
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_TravelDateStart

-- Travel End --   date
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_TravelDateEnd

-- Service start --    date
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_ServiceDateStart

-- Service End --    date
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: New_ServiceDateEnd

-- SPFTRecordID--						 BIGINT (this is the record id of the primary SPFT table record)
				--						Vintage CRM Mapping:  
										Table: New_servicetrip
										Column: new_HospitalConformedReceiptofMedicalLicensin

-- ArrivalAtHospital --    date
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: new_ArrivalatHospital

-- DepartureFromHospital --    date
--						Vintage CRM Mapping:  
						Table: New_servicetrip
						Column: new_DeparturefromHospital

****/


--this is from Service-Trip window vintage CRM
SELECT * FROM SPDSDEV.dbo.SPFU_WMMServiceTripDates
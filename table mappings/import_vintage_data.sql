
/*SPFT_WMMServiceTrips table data to import*/
WITH SPFT_WMMServiceTrips (SpecialtyCode, PlacementCoordinator, 
							LogisticsCoordinator, AccountNumber,
							TripStatus, TravelStatus,
							PrimaryDoctor, Country,
							PrimaryTripID, SPGVRecordId, 
							SPFTRecordID)
AS
(

	SELECT st.new_specialtycodeidName,
			REPLACE(SUBSTRING(st.OwnerIdName, charindex(',', replace(st.OwnerIdName, ' ', '')) + 1, len(st.OwnerIdName)) 
			+ ' ' 
			+ left(st.OwnerIdName, charindex(',', st.OwnerIdName) -1),',','') AS PlacementCoordinator,
			
			REPLACE(SUBSTRING(st.new_secondaryuseridName, charindex(',', replace(st.new_secondaryuseridName, ' ', '')) + 1, len(st.new_secondaryuseridName)) 
			+ ' ' 
			+ left(st.OwnerIdName, charindex(',', st.new_secondaryuseridName) -1),',','') AS LogisticsCoordinator,
			a.AccountNumber,
			st.New_TripStatus,
			st.New_TravelStatus,
			c.New_AccountNumber,
			st.New_Country,
			st.new_primarytriprecordid,
			a.AccountNumber,
			st.New_servicetripId
	FROM dbo.New_servicetrip st
	LEFT JOIN dbo.Account a
		ON a.AccountId = st.new_account
	/***This Join is to get us the primary doctor account_number****/
	LEFT JOIN dbo.Contact c
		ON c.ContactId = st.new_primarydoctorid
	GROUP BY st.new_specialtycodeidName,
			 st.OwnerIdName,
			 st.new_secondaryuseridName,
			 a.AccountNumber,
			st.New_TripStatus,
			st.New_TravelStatus,
			c.New_AccountNumber,
			st.New_Country,
			st.new_primarytriprecordid,
			a.AccountNumber,
			st.New_servicetripId


	


)
SELECT * FROM SPFT_WMMServiceTrips st



/*SPFW_WMMServiceTripLogistics table data to import*/
WITH SPFW_WMMServiceTripLogistics (WMMFlightAssistance, DoctorPurchasedFlights, 
							BibleSent, NameForBible,MedicalDocumentsSent, 
							HospitalConfirmedDocuments,SPFTRecordID)
AS
(

	SELECT st.New_logistics_wmmresponsibleforflights,
		   st.New_logistics_doctorpurchasedflights,
		   st.New_logistics_BibleSent,
		   st.New_logistics_nameonbible,
		   st.new_MedicalLicensingDocumentsSenttoHospital,
		   st.new_HospitalConformedReceiptofMedicalLicensin,
		   st.New_servicetripId AS SPFTRecordID
	FROM dbo.New_servicetrip st
	GROUP BY st.New_logistics_wmmresponsibleforflights,
		   st.New_logistics_doctorpurchasedflights,
		   st.New_logistics_BibleSent,
		   st.New_logistics_nameonbible,
		   st.new_MedicalLicensingDocumentsSenttoHospital,
		   st.new_HospitalConformedReceiptofMedicalLicensin,
		   st.New_servicetripId
)
SELECT * FROM SPFW_WMMServiceTripLogistics tl

/*SPFV_WMMServiceTripDetails table data to import*/
WITH SPFV_WMMServiceTripDetails (DailyRate, SPFTRecordID, 
							SecondarySpeciality, ProjectOf,
							NumberofChildren,Comments,PAACS,
							ResidentFunding)
AS
(

	SELECT st.New_DailyRate,
		st.New_servicetripId AS spft_record_id,
		ns.New_name,
		st.New_ProjectOf, --Values: 1 = WMM, 2 = SP, 3 = No Trip Report
		st.New_NumberofChildren,
		st.New_Comment,
		st.New_RecordServicetoPAACS,
		st.New_ResidentFunding
	FROM dbo.New_servicetrip st
	LEFT JOIN dbo.New_specialization ns
		ON ns.New_specializationId = st.new_SecondarySpecialty
	GROUP BY st.New_DailyRate,
		st.New_servicetripId,
		ns.New_name,
		st.New_ProjectOf, --Values: 1 = WMM, 2 = SP, 3 = No Trip Report
		st.New_NumberofChildren,
		st.New_Comment,
		st.New_RecordServicetoPAACS,
		st.New_ResidentFunding
)
SELECT * FROM SPFV_WMMServiceTripDetails td

